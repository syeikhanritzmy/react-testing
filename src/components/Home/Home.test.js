import { render, screen } from '@testing-library/react';
import React from 'react';
import Home from './Home';
import { BrowserRouter } from 'react-router-dom';
test('renders halaman home dengan benar', () => {
  render(<Home />, { wrapper: BrowserRouter });
  const btn = screen.getByRole('button');
  expect(btn).toBeInTheDocument();
  expect(btn).toHaveTextContent('Users List');
});
