import React from 'react';
import { useNavigate } from 'react-router-dom';

export default function Home() {
  const navigate = useNavigate();
  return (
    <div className="container mx-auto">
      <h1 className="font-medium text-xl">Testing Example</h1>
      <div className="border-b-4 border-slate-500 border m-5"></div>
      <div className=" w-full p-5 bg-blue-300 flex flex-col items-start text-left gap-y-4">
        <h1 className="font-bold text-4xl"> why do we need Test ?</h1>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores ex
          quidem quae doloribus nam, ipsum itaque aspernatur laudantium, dolorum
          aperiam incidunt pariatur sequi sunt id temporibus esse deserunt
          facilis alias!
        </p>
        <button
          className="p-4 bg-blue-400"
          onClick={() => {
            navigate('users');
          }}
        >
          Users List
        </button>
      </div>
    </div>
  );
}
