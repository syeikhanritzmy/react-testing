import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
const API = 'https://jsonplaceholder.typicode.com/users?_start=0&_limit=10';

export default function Users() {
  const [comments, setComments] = useState();
  const navigate = useNavigate();
  const getAPI = async () => {
    try {
      const response = await fetch(API);
      const data = await response.json();
      console.log(data);
      return setComments(data);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    getAPI();
  }, []);

  return (
    <div className="container mx-auto w-full">
      <button
        className="bg-green-400 text-white p-5"
        onClick={() => {
          navigate('/', { replace: true });
        }}
      >
        Back to Home
      </button>
      <table className="table-auto w-full border-collapse border-slate-400 border-2 ">
        <thead className="bg-black text-white">
          <tr>
            <th className="border-b-2 border-slate-400">ID</th>
            <th className="border-b-2 border-slate-400">Name</th>
            <th className="border-b-2 border-slate-400">Username</th>
            <th className="border-b-2 border-slate-400">Email</th>
          </tr>
        </thead>
        <tbody className="text-center">
          {comments?.map((data, index) => (
            <tr
              key={index}
              className={index % 2 === 0 ? `bg-slate-100` : 'bg-white'}
            >
              <td>{data.id}</td>
              <td>{data.name}</td>
              <td>{data.username}</td>
              <td>{data.email}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}
