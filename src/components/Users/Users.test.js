import { render, screen, waitFor } from '@testing-library/react';
import Users from './Users';
import { BrowserRouter } from 'react-router-dom';

test('renders halaman detail dengan benar', async () => {
  render(<Users />, { wrapper: BrowserRouter });
  // search button back
  const btn = screen.getByRole('button');
  expect(btn).toBeInTheDocument();
  expect(btn).toHaveTextContent('Back to Home');

  const user = await waitFor(
    () => {
      return screen.findByText('Leanne Graham');
    },
    { timeout: 5000 }
  );
  expect(user).toBeInTheDocument();
});
