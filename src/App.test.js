import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';
import { BrowserRouter } from 'react-router-dom';
test('Render ', () => {
  render(<App />, { wrapper: BrowserRouter });

  const title = screen.getByText(/why do we need test ?/i);
  expect(title).toBeInTheDocument();
});
