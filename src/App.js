import Home from './components/Home/Home';
import Users from './components/Users/Users';
import { Routes, Route } from 'react-router-dom';
function App() {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="users" element={<Users />} />
    </Routes>
  );
}

export default App;
